---
title: Death Mark
image: /img/Death_Mark.png
tags:
- Damage Items
---

{{< figure src="/img/Death_Mark.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: “Death Mark”
Tracking Number: 66******
Estimated Delivery: 02\22\2056\
Shipping Method:  High Priority
Shipping Address: 421 Lane, Lab [72], Mars
Shipping Details:

Everyone said that I was crazy to search for lost artifacts on Mars. Idiots. There hasn’t been any proof of a previous civilization - but I’ve always trusted my gut. This skull proves that I’m right - that something did exist here before.

That smug professor at the university... always disparaging my research. I loved seeing the look on his face as I shook his hand. Idiot. Karma must have been working overtime - I heard he fell ill shortly after. I suppose my success was just too much for him.

...In fact, everyone I’ve shown seems to not be returning my calls. Are they avoiding me? Are they scared this news would shake up their academic communities? Too proud to admit I’m right?

I’ll find someone who will give me the recognition I deserve. I’ve worked too hard and done too much. If I don’t keep going, I think I might just die.</pre>
{{< /rawhtml >}}

