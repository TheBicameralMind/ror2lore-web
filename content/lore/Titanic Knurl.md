---
title: Titanic Knurl
image: /img/Titanic_Knurl.png
tags:
- Utility Items
---

{{< figure src="/img/Titanic_Knurl.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Lab Dissection Analysis File</span>

Subject: Titanic Knurl
Technician: C. Foreman
Table Spec: Mineral Analysis BFC-5
Notes: 

&gt; From initial inspection, Knurl seems to be comprised of non-metallic substances. No marks are left when Knurl is r믭 against test surface.
&gt; We inspect hardness of Knurl. We managed to chip some of the Knurl away, showing us that the Knurl was tough but granular – individual fragments could be removed with little effort.
&gt; The fragment is moving. It appears to be trying to rejoin the mass.
&gt; Out of curiosity, I let it move freely. It slides along the table, up the knurl, and deposits itself back to its original position. The seam lines fade and the knurl is back as it had been minutes ago.
&gt; I test for magnetic properties in the rock. None are found.
&gt; Knurl’s regenerative properties are documented, but are unexplainable at this moment.
&gt; Knurl is slowly moving off the table as I write this.</pre>
{{< /rawhtml >}}

