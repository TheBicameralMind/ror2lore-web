---
title: Crowbar
image: /img/Crowbar.png
tags:
- Damage Items
---

{{< figure src="/img/Crowbar.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">//<span>--</span>AUTO-TRANSCRIPTION FROM CARGO BAY 5 OF UES [Redacted] <span>--</span>//</span>

“Hey, toss me a crowbar?”

Jesse walked over to the toolbox and rummaged around. Plucking a crowbar from the container, she went over to James.

“Thanks,” he grunted, attempting to pry the chest open. Jesse sat herself on the table next to James, idly watching him work. “I swear, this had to be the one chest that CAN’T be paid open... Crash must have... broken the circuitry inside.” James mumbled to himself.

“Yeah.” Jesse looked around, her eyes falling on a palette of chests that had been recovered from the recent expeditions. Sighing, she hopped to her feet. “I’m heading to the mess hall. Want anything?” 

“Yeah, how about a better crowbar? This thing is only a second edition.” James stretched, tossing the crowbar over his shoulder. Patting the still unopened chest, he chuckled. “It’s good, but to be honest… it’s getting a little old.”

Jesse laughed.</pre>
{{< /rawhtml >}}

