---
title: Corpsebloom
image: /img/Corpsebloom.png
tags:
- Healing Items
---

{{< figure src="/img/Corpsebloom.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span>========================================</span>
<span>====</span>   MyBabel Machine Translator   <span>====</span>
<span>====</span>     [Version 12.45.1.009 ]   <span>======</span>
<span>========================================</span>
Training… &lt;100000000 cycles&gt;
Training… &lt;253421 cycles&gt;
Complete!
Display result? Y/N
Y
<span>========================================</span>

The body is received and placed in a special containment unit. Proper care must be taken to ensure most efficient growth conditions.

The seeds are planted in the body.

One day passes.

Flesh softens and stretches. Youth returns to the face. Bones harden.

Another day passes.

Dead skin falls away from the body, and muscles gain strength. Hair resumes its original luster.

Another day passes.

The flowers have fully bloomed and the body is as good as new.

No signs of life, but not necessary.</pre>
{{< /rawhtml >}}

