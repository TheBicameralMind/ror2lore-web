---
title: Spinel Tonic
image: /img/Spinel_Tonic.png
tags:
- Equipment
---

{{< figure src="/img/Spinel_Tonic.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“Reality is whatever the mind decides it to be. Take a sip of the drink, and the mind becomes malleable. From there, you can shape it into whatever form you please... and the world around you follows your example.”

- Sigibold the Drunken</pre>
{{< /rawhtml >}}

