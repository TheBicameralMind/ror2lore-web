---
title: War Horn
image: /img/War_Horn.png
tags:
- Damage Items
---

{{< figure src="/img/War_Horn.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"The War of 2019, while lasting only a brief year, was the bloodiest conflict in human history. As the war got deadlier throughout the year, many rebel groups began to rely on tradition and history for inspiration. 

The War Horn, pictured above was a favorite of the Northern Fist Rebellion for both its inspirational and tactical uses."

-Exhibit at The National WW19 Museum</pre>
{{< /rawhtml >}}

