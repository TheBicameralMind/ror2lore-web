---
title: Lost Seer's Lenses
image: /img/Lost_Seer's_Lenses.png
tags:
- Damage Items
---

{{< figure src="/img/Lost_Seer's_Lenses.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"What does it mean to see?

To a beast, it means to identify potential food and mates. To propagate, to survive. Sight is nothing more than a means to an end, is it not?

To someone like me... To those who know true purpose... To see is to LIVE. To see is to spot that one little crack in the enemy's armor, to spot that smallest moment of hesitation when they rear back to strike.

To see the way flesh folds once the skull gives in to your weapon.

Yes, it's the little things, but seeing those things... REALLY seeing them. Ah, it fulfills you. I do hope you understand... And if not, you will eventually, if you've read this note."

- Lost Journal, recovered from Petrichor V</pre>
{{< /rawhtml >}}

