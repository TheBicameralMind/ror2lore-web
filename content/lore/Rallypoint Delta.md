---
title: Rallypoint Delta
image: /img/Rallypoint_Delta.png
tags:
- Environments
---

{{< figure src="/img/Rallypoint_Delta.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore">“Hey, if anyone can- My name is Malik Dhillon. I was aboard the Contact Light when, oh god- The ship crashed, marooning us here. I’ve never seen anything- I’ve never seen anything like this. Monsters, terrifying monsters, coming at us from all sides. We set up a base, to try and gather up numbers... But it didn’t matter. There’s, there’s too many of them! I don’t think we can last. If anyone gets this message, please, send help to these coordinates! They’re uh, they’re embedded in the message’s frequency. They should be... I don’t think we can last much-“

-Signal echoes, UES Contact Light</pre>
{{< /rawhtml >}}

