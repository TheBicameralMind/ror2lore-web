---
title: Oddly-shaped Opal
image: /img/Oddly-shaped_Opal.png
tags:
- Utility Items
---

{{< figure src="/img/Oddly-shaped_Opal.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">//<span>--</span>AUTO-TRANSCRIPTION FROM UES [Redacted] <span>--</span>//</span>

"...You think this planet is as bad as they say?"

Liz sat in silence among her fellow soldiers. She, like the rest of her squadron, had been taken from the middle of a firefight in the galactic outback and brought before an old UES veteran. Liz was used to debriefing by now, it all blended together in her head. Something about monsters, and the missing UES Contact Light.

"...Dunno." Liz murmured as she turned a small, oddly-shaped hunk of opal in her hand. It was one of the only things she had that reminded her of Parker. Of when things were calm, and peaceful.

"...Heh, I don't think a shiny rock will do much," A soldier joked. Liz's brow furrowed under her helmet. "Yeah, probably not... but..."

Liz took a deep breath and slipped the opal back into her pocket. "It just helps."</pre>
{{< /rawhtml >}}

