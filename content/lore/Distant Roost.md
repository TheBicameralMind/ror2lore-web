---
title: Distant Roost
image: /img/Distant_Roost.png
tags:
- Environments
---

{{< figure src="/img/Distant_Roost.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
$ Combing for relevant data... done.
Complete!
</span>

“Captain’s Log. Date: [REDACTED]. Time: [REDACTED], Galactic Standard.”

“Several shuttles have been dispatched and landed on the planet. We’ve also deployed some drop pods for the less… social, of our crewmembers. And, well...” A chuckle can be heard. “I have considered joining them on the planet, to oversee operations and manage the chain of command.  This has been my first deployment in many years. I’m not going to sit back and rot on the bridge like some old hardware. That said...”

The old man clears his throat, getting back on topic. “The shuttles landed in a mountainous region, along some cliffs. While disembarking and setting up a rallypoint, we came under attack from local fauna, just as reports detailed. The situation quickly escalated, and within the first 24 hours we have sustained major casualties and injuries.”

 “Thankfully the ammunition and armaments I requested were granted by UES, as more of the crew has reported back that all outer life encountered thus far has been hostile. Some of our ground forces have also reported strange and irregular natural phenomenon.”

The old man gazes out of the window of his cabin, the moon shining in the night sky. “What a strange planet. This’ll certainly make a good story.”</pre>
{{< /rawhtml >}}

