---
title: MUL-T
image: /img/MUL-T.png
tags:
- Survivors
---

{{< figure src="/img/MUL-T.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"OK. Everything checks out. You're free to go, pal."

The robot chirped in acknowledgement. It folded neatly unto itself and began rolling down the hangar's exit lane - at a very safe pace.

Hiroshi pulled a switch. The ceiling rail sprung to life, with a distinct mechanical chatter.

"So, what's wrong with this one?" Rico enquired with minimal interest, seemingly distracted by the novel he had buried himself in.

"Looks like cosmetic damage on a MUL-T unit. Got dinged up by another unit in the charging bay that malfunctioned after another power surge. We'll be checking that one out after this. You can thank the electrical crew for that," Hiroshi remarked. 

"Great. How hard is it for them to just keep things stable? They have robots doing 90% of their job anyway." Rico scoffed.

"We've got complaints on this unit about poor welding jobs, which might've been the cause. Optical calibration is probably off, so we'll also be taking a look at that while it's here. I'd still put money on it being electrical not keeping up with maintenance. Hey, Rico! Would you put that down and help me out?"

The rail brought a bright yellow robot, slumbering, into the hangar. Hiroshi flipped the switch and the rail slowed to a halt, bringing with it silence. He began moving to unhook the robot. Reluctantly, Rico set his book on the work desk and started to help unload. In a few minutes the robot was on the service platform.

Hiroshi walked over to one of the tool cabinets and pulled out a strange device. He popped open the access panel to the unit's dented head, and began probing at the damage from the opposite side. In no time at all, the tool beeped and kicked back violently. The dent was gone.

"Go put this away while I fix the optics," Hiroshi said, one arm stretched behind his back, device in hand. "I have something I want to show you in a sec."

Rico grunted and took the de-denting tool back to the cabinet. Hiroshi was already soldering away with the iron from his pocket.

This caught Rico's interest. 

"You don't need a soldering iron to fix the calibration. What are you doing?" he asked.

"I already fixed the optics. Remember when I said I wanted to show you something cool - the next time we got a MUL-T in the shop? Come here." said Hiroshi, gesturing. "You see these two chips? It’s actually just one chip that's been split. MUL-T’s come with the same cores as those fancy chef bots, but they cut off access to the main learning module."

"You don't say," Rico responded, still trying to see the chip Hiroshi was talking about. "Why'd they do something like that?"

"It's cheaper to just make all the cores the same way at the factory and alter them later. The manufacturer says it's for safety, but they just use it to sell you the new cores every few years instead of letting you just teach the units to do a new job." Hiroshi explained.

"Sounds about right. If it were a safety issue, why don't they do the same thing with the chef bots? They've been super stable. Can't say they've even ever got an order wrong, even if it's totally out there. They even managed to get Ma's Squid Risotto right."

"It's what they can get away with. Anyway, that should do it. We'll just sneak a few minutes with this guy. Wake up," he commanded.

The robot's relaxed posture disappeared, and it stood fully alert.

"Hey pal, you see this thing I'm doing with my hand? Can you do that?"

The robot mimicked a rude gesture.

"That's fantastic! You're doing great!" Hiroshi was excited, and Rico got a devilish grin. He rushed off to his desk. Hiroshi continued repeating the gesture, and the robot continued mimicking it back with its single functional hand. Rico stepped back into the robot's view, with a picture covered in holes. "Can you remember this man? This is Ron from corporate, and he's a total degenerate."

The robot beeped in affirmation.

"Can you do that next time you see this man?"

The robot beeped in affirmation again.

"Rico! He's gonna remember that! MUL-T, forget that."

The robot did not respond.

"What are you two up to now?" said a grating voice, clearly approaching from no discernable direction. In a panic, Hiroshi slapped the access panel on the unit's head closed and covered its optical sensor.

"Just fixing up robots! Doing our job! In fact, we just finished with this one. You're good to go, pal!" Hiroshi exclaimed to Ron, slapping the robot on the back. The machine folded up neatly into itself and began rolling down the hangar's exit lane.

"I see. I'd better not catch you two slacking again." Ron said sternly, before turning around and making his usual relieving exit – most likely to pester other workers on his route.

They both let out a sigh of relief. Rico stood up. "Well, we're due at the site in about… 13 hours. That's enough time to grab that MUL-T, pop it back open, and reset it."

Hiroshi also stood up. “Yeah, I’m not too worried. How much trouble could it cause in 13 hours?”</pre>
{{< /rawhtml >}}

