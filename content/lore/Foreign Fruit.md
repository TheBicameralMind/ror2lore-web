---
title: Foreign Fruit
image: /img/Foreign_Fruit.png
tags:
- Equipment
---

{{< figure src="/img/Foreign_Fruit.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“The Nutapple is an exotic fruit picked from the curious Xelphum Tree, native to coastal Gemines on Titan. Renowned for its sweet, yet cleansing flavor (and low caloric total), the Nutapple is a must-have for health enthusiasts everywhere.”

- Dietary Sciences Digest</pre>
{{< /rawhtml >}}

