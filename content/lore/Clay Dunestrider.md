---
title: Clay Dunestrider
image: /img/Clay_Dunestrider_-_Logbook_Model.jpg
tags:
- Bosses
---

{{< figure src="/img/Clay_Dunestrider_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">$ Transcribing audio... done.
$ Resolving... done.
$ Outputting text strings... done.
Complete!
</span>

&lt;Click&gt; There are no landmarks in site. I am surrounded by infinite desert. &lt;Click&gt;

&lt;Click&gt; I’ve run out of rations… and no one is responding to my transponder. I may be doomed if I don’t find a way out of these dunes. &lt;Click&gt;

&lt;Click&gt; I see something in the distance… some large creature. I may be in luck yet. &lt;Click&gt;

&lt;Click&gt; Oh... my eyes deceived me. It was not a creature… but a herd. There was more than one. Many more. 

Hundreds of enormous creatures, moving as a mob– and each one the biggest beast I have ever seen. These striders resembled… enormous spiders, or hermit crabs, in a porcelain shell, with legs dark like tar. 

Between the sound and the sand, the entire herd bounded past me without realizing – moving at incredible speeds on their long legs. Were they fleeing… or migrating? I still can’t believe… what a terrible place. Get me out. &lt;Click&gt;

&lt;Click&gt; I still have no food. &lt;Click&gt;</pre>
{{< /rawhtml >}}

