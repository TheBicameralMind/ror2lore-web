---
title: Symbiotic Scorpion
image: /img/Symbiotic_Scorpion.png
tags:
- Damage Items
---

{{< figure src="/img/Symbiotic_Scorpion.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"There's countless ways to defeat your enemy. You can shoot them. You can punch them. Beat them, mash them, stick them in a stew, list goes on. But man, one thing that really makes me laugh...? That's right:

Burying them under a mound of scorpions."

"You're messed up in the head, Bill."

- Signal echoes, UES Contact Light</pre>
{{< /rawhtml >}}

