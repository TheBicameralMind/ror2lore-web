---
title: Void Fiend
image: /img/Void_Fiend.png
tags:
- Survivors
---

{{< figure src="/img/Void_Fiend.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Darkness shrouding darkness - echoes from futures past and past futures and places and times eeking out from between cracks in reality.  A suffocating prison of the mind and soul, occupied by beasts poking and prodding at your spirit forever.  Forever testing you.  Forever ?T?E?MPTING? you, iteration after iteration.

They've run laps through the brain, ?CRAWLE?D? every last synapse for memories and data: weapons to use against theoretical armies with more resilient hearts and evasive bodies.  ?CH?TTERIN?G? and clacking they can be heard watching from beyond the cell, no longer interested but still patrolling the grounds.  Here in this purgatory there is no hunger and no thirst - all that remains are the ?ECH?OES??.

?H?OW L??ONG?? has it been?  Time seems to warp in and out around breaths circulating ???W?THIN???.  Darkness all-consuming starts to ?D??ROW?N? alveoli and ?FLOO?D? inky black veins.  Memories ??TR?ESPASSING? into reality as reality fades into the distant past.  ???SILENC?E?? broken only by the sound of watching eyes.

????SH?AKE THE ?BARS????.

Twisting and contorting, engulfed in ??F?EAR??? and becoming the embodiment.  Reborn like some sick prophecy, arms and legs like new corpses on the seabed.  No longer ?SUP??RESSE?D?

?H?UNGER?  ??THI?RST?  ?H??OPE?

<span class="mono">?CONSUME?</span></pre>
{{< /rawhtml >}}

