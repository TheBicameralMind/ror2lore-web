---
title: The Back-up
image: /img/The_Back-up.png
tags:
- Equipment
---

{{< figure src="/img/The_Back-up.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"The inclusion of Rapid Response Drone Squadrons (RRDS) by local law enforcement marked a dire turning point of Plank's Rebellion. After its massive "success" in quelling the Rebellion - and subsequent implementation across Mercury - underground drone hackers became highly sought after by black market users."

-Overview of Drone Technology, Vol.2</pre>
{{< /rawhtml >}}

