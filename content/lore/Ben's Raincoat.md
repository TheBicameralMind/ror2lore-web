---
title: Ben's Raincoat
image: /img/Ben's_Raincoat.png
tags:
- Utility Items
---

{{< figure src="/img/Ben's_Raincoat.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Return: Gorton Men's Rain Jacket (Medium, Yellow)
Tracking Number: 73***********
Estimated Delivery: 03/04/2053
Shipping Method:  Standard
Shipping Address: 545 Matthaios Blvd., Gloucester, Earth
Reason For Return: 

The jacket itself is fine, but for some reason, a bunch of the students here got super weird about me wearing it. They keep telling me to take it off because this other guy, Ben, wears a yellow jacket. I guess it's kinda his thing or something.

Refund:
Replace:  X

Additional Notes:

Please send a replacement that's blue.</pre>
{{< /rawhtml >}}

