---
title: Paul's Goat Hoof
image: /img/Paul's_Goat_Hoof.png
tags:
- Utility Items
---

{{< figure src="/img/Paul's_Goat_Hoof.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Paul’s Goat Hoof
Tracking Number: 187*****
Estimated Delivery: 03/27/2056
Shipping Method:  Standard
Shipping Address: 2663rd Fields, Redmond, Venus
Shipping Details:

A hoof from one of my many goats; I noticed one day that one of my goats had an abnormally large foot. Thinking it was cancerous, I went to the doctors and lo-and-behold - it was. My goat died shortly after.</pre>
{{< /rawhtml >}}

