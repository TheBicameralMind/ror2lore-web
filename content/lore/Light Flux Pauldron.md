---
title: Light Flux Pauldron
image: /img/Light_Flux_Pauldron.png
tags:
- Utility Items
---

{{< figure src="/img/Light_Flux_Pauldron.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"The wind guides us all. Listen to the song of the wind, and dance according to its instruction. You will find yourself dodging every blow, countering every strike. You will find that the wind will lift you up, making you as free as a fallen autumn leaf. But heed, and do not lose yourself to the wind. Running is a viable option, though eventually you will find yourself with nowhere left to run to."

-Will of Combat, Second Excerpt</pre>
{{< /rawhtml >}}

