---
title: Wandering Vagrant
image: /img/Wandering_Vagrant_-_Logbook_Model.jpg
tags:
- Bosses
---

{{< figure src="/img/Wandering_Vagrant_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
$ Combing for relevant data... done.
Complete!
</span>
This is the logbook entry of D. Furthen, naturalist for the UES [Redacted] in conjunction with the UES Research and Documentation of Outer Life. I am joined by my good friend Tharson, who is keeping me safe through this journey.

<span>---------------------</span>
We documented our first family of outer life. A relative of the previously documented Icarian Jellyfish, this specimen dwarves its brethren several times over.

I will describe its properties below. I have assigned its common name as “Icarian Vagrant”.

* Massive in size. I wouldn’t believe the Vagrant is capable of flight had I not seen it myself. Like its smaller cousin, the Vagrant’s hull contains buoyant gases that allow it to fly. 

* These gases are easily excitable and produce incredible amounts of static electricity, cloaking the Vagrant in a protective aura that scorches anything that approaches. The Vagrants can also hurl charged nematocysts as a method of self-defense, as evidenced when I asked Tharson to approach a lone Vagrant. Tharson is OK from this encounter.

* The name of “Vagrant” was decided on by its nomadic nature. We trailed a lone specimen for several days. The Vagrant would wander from place to place, occasionally dipping down towards the ground to feel around with its tentacles. After a few minutes, it would ascend and continue on its path. Could the Vagrant be looking for something, indicating intelligence, or is this merely a hunting method?

* When the clouds part, dozens of Vagrants can be seen lazily drifting throughout the sky, twinkling like stars. Despite their beauty, it raises a horrifying question – are the stars at night on this planet truly stars, or just more Vagrants? The answer can spell truly horrifying implications for our stay on this planet.</pre>
{{< /rawhtml >}}

