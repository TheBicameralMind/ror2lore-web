---
title: Radar Scanner
image: /img/Radar_Scanner.png
tags:
- Equipment
---

{{< figure src="/img/Radar_Scanner.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Radar Communications Array
Tracking Number: 02****************
Estimated Delivery: 01/14/2056
Shipping Method:  Standard
Shipping Address: PO Box 25-B, Fort G.B., Mercury
Shipping Details: 

You’re asking for trouble, ordering this through the UES. Not very covert. This thing can put out anything from infrared to UV waves – maybe it’ll let you see the Enforcers coming to haul you off to jail as they kick down your door! Hah!</pre>
{{< /rawhtml >}}

