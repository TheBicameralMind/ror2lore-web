---
title: Newly Hatched Zoea
image: /img/Newly_Hatched_Zoea.png
tags:
- Damage Items
---

{{< figure src="/img/Newly_Hatched_Zoea.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Lab Dissection Analysis File</span>
Subject: Newly Hatched Zoea
Technician: X. Shun
Table Spec: BioSaf, spectroscopic hood enabled
Notes:

- Taking a moment to contain myself.
- It's been quite a while since we've received a sample<span>--</span>INTACT sample<span>--</span>so I am being abundantly cautious.
- Sample is in excellent condition.  I am pleased with the care Jiminez took compared to her predecessor.
- Sample appears to be an undocumented carcinized species.  Perhaps a juvenile?
- Attempting to biopsy epidermis
- Exterior of the sample is too hard for my scalpel
- I believe I have found the oral cavity.  Attemping oral biopsy.
- <span class="mono">//<span>--</span>AUTO-TRANSCRIPTION FAILED <span>--</span>//</span>
- <span class="mono">//<span>--</span>AUTO-TRANSCRIPTION FAILED <span>--</span>//</span>
- Help
<span class="mono">End Of File</span></pre>
{{< /rawhtml >}}

