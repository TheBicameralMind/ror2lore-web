---
title: Beads of Fealty
image: /img/Beads_of_Fealty.png
tags:
- Items
---

{{< figure src="/img/Beads_of_Fealty.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">The master of this world is a benevolent protector. Our savior. Despite the natures of its various inhabitants, he has made a peaceful place here. 

Peace comes at a cost, of course. Some things are strictly forbidden. Above all, there is one item that cannot be tolerated – to possess it is to surely perish: a strange, heavy, and deeply entrancing set of beads that seem to speak with whoever holds them. 

To discover them among one’s community is to create a dark, rippling panic. It is not long after they are found that Providence, the mighty protector, appears in a thunderous instant to pursue their owner and solemnly take a life he swore to defend.

It is no physical property of the beads themselves which provoke such a drastic response - but instead, what they represent. They are a symbol, and to carry them is a dark promise to undo the world – to cast to oblivion all people who find their final place here, all in exchange for a doomed return to ruinous way. The elusive, sinister intelligence with whom these beads represent a pact seems known only to the bearer - and to Providence himself.

One thing, however, is known to anyone who has borne witness to these beads: a cruel, cackling laughter that erupts as blood and coins of favor are spilled, abruptly silenced as the beads are destroyed. Where they come from isn’t known, but should you encounter these beads you must leave them far, far behind. 

Never look back.</pre>
{{< /rawhtml >}}

