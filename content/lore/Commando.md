---
title: Commando
image: /img/Commando.png
tags:
- Survivors
---

{{< figure src="/img/Commando.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">The soldier ducked, barely getting under cover as the wall behind him exploded. His head was pounding - his muscles aching. But now was not the time for rest.

He counted the seconds. The mounted missile launchers always took a few seconds to recalibrate, and if he took advantage of that time...

Through the sounds of gunfire and chaos, he heard a small click as the enemy missile platform prepared for a second strike. Recognizing his opportunity, the soldier jumped up from behind the wall and fired his pistol, landing a shot straight between the eyes of the platform’s operator. 

His shoulders tightened as he braced for recoil: now was not the time for rest. Glancing down the battlefield, there were only a few more enemy platforms to take, and this front would be secured.

But before he could move to join his platoon, he received a notification on the heads-up display in his helmet.

<span class="mono">YOU HAVE BEEN REASSIGNED. REPORT TO COMMAND FOR YOUR NEW ASSIGNMENT.</span>

Looks like the time to rest wasn’t coming any time soon.</pre>
{{< /rawhtml >}}

