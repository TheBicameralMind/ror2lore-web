---
title: Stone Titan
image: /img/Stone_Titan_-_Logbook_Model.jpg
tags:
- Bosses
---

{{< figure src="/img/Stone_Titan_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">“A flash of red lightning, and a hillside moved to form this gargantuan stone warrior. A larger cousin of the Stone Golem (see Chapter 13), this mighty Titan commands not just lesser stone constructs, but the earth itself. Shaping rudimentary weapons out of dirt, the Titan is capable of striking from any distance with incredible force. Though, reflecting on the Titan’s immense power, that leads me to think... For what purpose do these constructs serve? 

Compared to the Golems of the planet, the Titans do not seem to be created on the spot, but emerge from underground. Why are they buried underground? Are they kept there for safe-keeping, or are they imprisoned? Were they created as weapons, or as guardians? What happened to their creators? Further analysis of the planet will hopefully shed some light on these mysteries.”

- The Mysteries of Petrichor V, First Draft</pre>
{{< /rawhtml >}}

