---
title: Ukulele
image: /img/Ukulele.png
tags:
- Damage Items
---

{{< figure src="/img/Ukulele.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">//<span>--</span>AUTO-TRANSCRIPTION FROM RALLYPOINT [Redacted] <span>--</span>//</span>

The planet was unforgiving, taking more and more with each passing day. Lives and limbs were lost, and the medical tent grew increasingly busy.

Jans was unfortunate enough to walk right into a group of Lemurians during a routine check around the rallypoint perimeter. He was lucky enough to escape with his life, but the Lemurians wounded his leg before backup arrived. 

Jans leaned back in his cot. He strummed the strings on his ukulele – which he had picked up from an earlier expedition – as he contemplated his thoughts. Nothing too complex, but just a simple tune that he had picked up back when he took lessons as a child. Unbeknownst to him, one by one, he had gathered himself an audience. The gentle melodies of Jans’ ukulele brought a rare comfort to the pained and labored patients, and for the first time since admittance, they felt like they could smile.

Life in the medical tent was never something anybody could look forward to. Death, irreversible injuries, and a choking atmosphere surrounded them. But for a brief moment… the lives of those patients became that much brighter. Jans stopped, surveying the crowd intently watching him. 

He laughed, readying himself for an encore. Maybe things wouldn’t be so bad after all.</pre>
{{< /rawhtml >}}

