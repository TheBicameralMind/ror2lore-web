---
title: Void Infestor
image: /img/Void_Infestor_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Void_Infestor_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">POTENTIAL INFESTOR SIGHTING REPORTS

REMINDER: ALL REPORTS ARE TO BE COLLECTED AND SHARED AMONG THE CREW IMMEDIATELY.  

REMINDER: ANY CONTACT WITH INFESTORS RENDER TEAMMATES UNSALVEAGABLE. SHOOT ON SIGHT.

<span>--</span> 1/23 <span>--</span>

Have you noticed the beasts near the outpost acting strangely?  There's been constant in-fighting the past few days, and some of them in particular have been acting erratically.  It never lasts long - the families tend to surround them and tear them to shreds before I can get much of a good look.  Do you think there's something wrong with the individuals? Or are those families detecting some kind of disease and stamping it out?  We've recovered only one of the corpses, and it looks like your basic lemurian, other than its mangled limbs and smashed in skull.  I've contacted Tharson to let him know that it'd be worth checking out,  I sure as hell know I'm not going to head into those enclaves while this wild civil war is going on.  Maybe this is just what lemurians get like when they have rabies, though its the first instance I've ever seen of something like rabies out here.

<span>--</span> 1/25 <span>--</span>

Familial in-fighting has gotten significantly worse.  Often the bouts last for much longer than only one kill now, and the factions seem to change erratically, as if driven by some sort of twisted bloodlust.  A group of 4 will be thrashing an individual to death, only to to turn their hatred toward another in the group soon afterwards.  This is the first time I've seen small groups totally wipe themselves out, only for the last one standing to hobble away on shattered legs.  I don't dare fire from my post, they don't seem interested in us and I'd like to keep it that way.  As far as I'm concerned, these neighbors of ours could drive themselves into extinction and it'd be a good day for me.  Atleast then I could sleep without worrying about the tent burning down every night.

<span>--</span> 2/?? <span>--</span>

&gt; AUDIO RECORDING RECOVERED FROM SITE 4A

Okay... okay...  Here, damned thing just record!  Just record!  Okay... light on?!  Good - okay.  Listen, this is [REDACTED], I want whoever finds this or finds me or my corpse or [REDACTED]'s corpse to know that I didn't go crazy.  Oh god... Okay, I just murdered [REDACTED].  Atleast, I think it was [REDACTED], it's very confusing.  Something like [REDACTED] was hobbling around the outpost at night - he moved like he didn't have bones or something - it doesn't matter.  I shouted out to him and he turned to fire his weapon toward me in the dark.  I dropped him dead and ... look I can't - if... if we're going to start doing this I just - I have to get out of here.  I just want you to know I'm not crazy.

<span>--</span> End of Recording <span>--</span>

&gt; Involved parties have been removed from record.


&gt;Full recording may be accessed on a need-to-know basis in report 596f757220707269736f6e.
<span>==================================================================</span></pre>
{{< /rawhtml >}}

