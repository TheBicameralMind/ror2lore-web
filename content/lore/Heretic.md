---
title: Heretic
image: /img/Heretic.png
tags:
- Survivors
---

{{< figure src="/img/Heretic.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">A blue flash appeared in one of the storage bays of the Contact Light. The chaos ensuing on the ship was a perfect distraction.

As soon as Kur-skan stepped out the portal she ducked into a sprint. She had no time. She weaved between gunfire and fireballs, between blasts of Vagrant energy and on-board security systems. They paid no mind to the feathered figure as she darted through closing blast doors.

She flew quickly down the halls of the ship. Despite it being her first time onboard a human vessel, she could sense the electromagnets in the walls – and where they all joined. She corked right, shooting into the ventilation shafts with a bang.

The captain barely had a chance to see her before she blasted sideways out into the control room. She tore out two windblades as he reached for his pistol. The captain fell to the ground in intervals. 

Her momentum crashed her into the control panels, both feet digging deep into monitors. She didn’t need those. Nimble, clawed fingers began ripping across keypads. She quickly split and rewired components, reaching into her feathered pouches for more bridges and other electronics. Various capacitors fell onto the floor. This ship was big, but not complex – and more importantly, it could still fly. But she had to go fast, or he –

And before she could finish her thought, he appeared in a flash of thunder. Blue and green light reflected off of his sword, cascading around the bridge. A cape fluttered, and Providence landed lightly on his feet.

Kur-skan bent her head backwards over her body, hands still tearing at panels. He was much faster than she expected. But maybe he would – 

Her last thoughts were interrupted instantly by a crystalline blade.</pre>
{{< /rawhtml >}}

