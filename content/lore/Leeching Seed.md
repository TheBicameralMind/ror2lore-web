---
title: Leeching Seed
image: /img/Leeching_Seed.png
tags:
- Healing Items
---

{{< figure src="/img/Leeching_Seed.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">It was a crisp evening - and things were looking up. The botanist began walking towards the Plant Biology Complex to check on several alien plants that had been recovered from the week before.

Another worker was already in the complex, busy at work.

“Hey.” “Oh, hello!” The worker greeted her enthusiastically. He was carrying several bags of fertilizer, and she gave him a quizzical look. “You need any help with those?” She said, gathering her clipboard for her check.

“Oh no, I’m alright. It’s a good workout, actually,” he said. As they walked, the botanist couldn’t help but wonder what kind of job would require THAT much fertilizer.

As the door to the greenhouse whirred open, she got her answer – roots scaling the height of the greenhouse and snaking along the ceiling, even invading the containers of other specimens. “What the hell!?” She cried, her jaw slack. And then she noticed the other plants. 

Every other plant in the greenhouse had withered – with an incriminating root leading back to the overgrowth that the worker was now feeding fertilizer. The botanist was speechless. “What happened...? What... what is that...?” She stammered. The worker turned to her and grinned.

“One of the species we got in the last expedition. He demands a lot of nutrients - it took me so long to get him to even sprout. I was wracking my brain, trying every trick in the book. And every other specimen seemed to be growing fine, so I figured, hey, why not share some of that? And look!”

Spreading his arms, the worker marveled at the gallery of dead plants. Dropping her clipboard, the botanist’s stomach sank. Things were no longer looking up.</pre>
{{< /rawhtml >}}

