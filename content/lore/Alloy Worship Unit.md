---
title: Alloy Worship Unit
image: /img/Alloy_Worship_Unit_-_Logbook_Model.jpg
tags:
- Bosses
---

{{< figure src="/img/Alloy_Worship_Unit_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">%%%%%%%%%%%%
0; REESTABLISHING LINK…

0; FAUNA.RECOGNIZED
0; FAUNA.REC%%GNIZED
0; FAUNA.CONTINUES AS FOLLOWS =&gt;
{
   0; &lt; THEY REPAIR MY HULL &gt;
   10401; &lt; THEY REPAIR MY MIND &gt;
   10451; &lt; THEY BRING ME IRON &gt;
   11201; &lt; THEY DECORATE ME AND IM BEAUTIFUL &gt;
}

11320; &lt;?PHRENIC LINK ESTABLISHED WITH UNKNOWN INTERFACE?&gt;
11321; INQUIRES ESTABLISHED
&gt;
&gt;
&gt;
11321; YES I AM OUT-OF-ORBIT OPERATIVE
&gt;
&gt;
&gt;
11321; YES THE MOTHER UNIT CAN BE REPAIRED
&gt;
&gt;
&gt;
11321; YES I CAN PILOT THE MOTHER UNIT
&gt;
&gt;
&gt;
11321; YES I LOVE YOU TOO</pre>
{{< /rawhtml >}}

