---
title: Overloading Worm
image: /img/Overloading_Worm_-_Logbook_Model.jpg
tags:
- Bosses
---

{{< figure src="/img/Overloading_Worm_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">“...And upon a great serpent, the hero did ride. Majesty and respect were his command, and so too his steed. A mighty creature, born of lightning and flame, bathing all in its presence in a magnificent blue glow. 

The beast stretched from one horizon to the next and belched storms that reminded all of the hero’s promise: that this land – and all those who live here - would never suffer again...”

- Galactic Fables, Volume IX</pre>
{{< /rawhtml >}}

