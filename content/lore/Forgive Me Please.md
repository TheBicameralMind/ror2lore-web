---
title: Forgive Me Please
image: /img/Forgive_Me_Please.png
tags:
- Equipment
---

{{< figure src="/img/Forgive_Me_Please.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">There is a note pinned to the doll:

Ayy, you’re killin’ me!

You’re killin’ me!

You’re killin’ me!

You’re killin’ me!

You’re killin’ me!

You’re killin’ me!

You’re killin’ me!

You’re really killing me.</pre>
{{< /rawhtml >}}

