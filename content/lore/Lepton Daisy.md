---
title: Lepton Daisy
image: /img/Lepton_Daisy.png
tags:
- Healing Items
---

{{< figure src="/img/Lepton_Daisy.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Lepton Daisy
Tracking Number: 23******
Estimated Delivery: 07/23/2056
Shipping Method: Standard
Shipping Address: Duck Lake, Orlandus, Earth 
Shipping Details:

Dear Stalnia,

I haven't been able to visit for a while, so I hope you and Libra are doing well. I heard that you're clearing the land there and want to spruce things up. I'm sending this flower I picked in the Carbon Fields of Electron Valley. The terraformers there told me that, over time, landscaping efforts were improved by the organic-band signals sent out by this plant. I've been helping them build a new world.

Just make sure to put it up high enough that Mushka can't get to it. I'm not sure what effect it'll have on dogs...</pre>
{{< /rawhtml >}}

