---
title: 57 Leaf Clover
image: /img/57_Leaf_Clover.png
tags:
- Utility Items
---

{{< figure src="/img/57_Leaf_Clover.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">It was a massacre. The orbital probes had incinerated both the battalion and the enemy– as well as the entire plateau. There was no life left to be found - except for one lone soldier.

The soldier trudged through the scorched plains, numbly looking for any survivors.

But he found none.

The soldier tried to find water – he had only just noticed how thirsty he was, and how much his head hurt. How much his heart hurt.

But he found none.

The soldier tried to remember, why he was out on this terrible mission, and why he had to say goodbye to so many.

But he found none.

The soldier tried to find anything – anything that could justify why he was alive. Some semblance of meaning – or hope.

And he found one.

A small leaf, peeking out from a pile of ash. He gently plucked the clover out of the ground – somehow, it survived hours of orbital bombardments. 

With clover in hand, the soldier began the long trek to report back to his superiors. Was this divine intervention? Was there some meaning – some reason as to why both him and this little clover survived? 

Or were they just lucky?</pre>
{{< /rawhtml >}}

