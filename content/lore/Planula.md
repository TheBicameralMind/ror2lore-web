---
title: Planula
image: /img/Planula.png
tags:
- Damage Items
---

{{< figure src="/img/Planula.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"Yes – the egg. The grandparent incubates the eggs."

"And the child?"

"The child lays the egg."

"And the egg becomes the…?"

"The parent."

"Okay. And the parent does what?"

"Takes care of children – and the grandparent."

"And the grandparent is the youngest?"

"Younger than the child, yes. But not the youngest."

"Who is the youngest?"

"The parent."

"…"</pre>
{{< /rawhtml >}}

