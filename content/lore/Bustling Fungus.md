---
title: Bustling Fungus
image: /img/Bustling_Fungus.png
tags:
- Healing Items
---

{{< figure src="/img/Bustling_Fungus.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">//<span>--</span>AUTO-TRANSCRIPTION FROM UES [Redacted] <span>--</span>//</span>

“You have a problem.”

“What? How? These are good for me, they…”

“Yeah, they help promote cell repair, carbon neutral, blah blah blah… I’ve heard that spiel a thousand times already. Whether they’re good for you or not, you can’t just have a diet consisting only of them. It’s basic dietary science!”

“So what?”

“So what? Eating only mushrooms isn’t healthy, just like eating only meat or vegetables isn’t healthy!”

“Oh, so you’re attacking vegetarians, now?”

“Wh- No! I just mean<span>--</span>”

“Well, when you’re a bit more tolerant of my lifestyle, I’d be glad to continue this conversation with you. Until then, I’d suggest you open your heart and appetite to a more… fungal… palette.”</pre>
{{< /rawhtml >}}

