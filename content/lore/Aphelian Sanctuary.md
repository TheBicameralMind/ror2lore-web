---
title: Aphelian Sanctuary
image: /img/Aphelian_Sanctuary.png
tags:
- Environments
---

{{< figure src="/img/Aphelian_Sanctuary.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">FIELDTECH Image-To-Text Translator (v2.5.10b)
# Awaiting input... done.
# Reading image for text... done.
# Transcribing data... done.
# Translating text... done. [3 exceptions raised]
Complete: outputting results.</span>

I have heard the young ones whisper amongst themselves, questioning why we must stay in our glistening towers. Why we must rigorously cleanse our bodies, why we must live rigorous, studious lives.

How uncouth. The younger generation knows not of the plight of Aphelia, before [The Hero] came to save us. They live thanks to him, thanks to our second chance...! They know nothing of strife, nothing of hunger, nothing of the [Tar]. That terrible substance that consumed our way of life, consumed our culture, consumed us down to the marrow.

Perhaps it is best they know nothing of our past, perhaps it is best that they remain clean.

Yes. Perhaps it is best.

...That won't stop me from grumbling at them. Stupid kids.

<span class="mono">Translation Errors:&lt;/style
# [Tar] could not be fully translated.
# [The Hero] could not be fully translated.</span></pre>
{{< /rawhtml >}}

