---
title: Lysate Cell
image: /img/Lysate_Cell.png
tags:
- Utility Items
---

{{< figure src="/img/Lysate_Cell.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"Recycling is an important thing. And I don't mean recycling a glass bottle to help the environment - though that's important too! No, by 'recycling,' I mean taking one thing, and using it for another. For example, my Ukelele. I love this thing. It has electric properties, and passes them on to my attacks - free chain lightning! But what if I were to 'recycle' it? I think the Uke would make a pretty good melee weapon. Imagine, bludgeoning a Lemurian and pumping it full of volts, in one easy step.

Do you get what I'm saying? There's opportunities for recycling all over the place. You just need to open your mind to the possibilities, and see... And do a little recycling. It's good for the environment."

- Lost Journal, recovered from Petrichor V</pre>
{{< /rawhtml >}}

