---
title: Recycler
image: /img/Recycler.png
tags:
- Equipment
---

{{< figure src="/img/Recycler.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: “TY-6G Recycler”
Tracking Number: 02******
Estimated Delivery: 11\06\2056\
Shipping Method: Standard
Shipping Address: 700 Baler Ave, Seattle, Earth
Shipping Details:

Standard issue recycling apparatus TY-6G. Please read the manual before operating. KEEP HANDS CLEAR OF THE TOP OPENING.

Use included recycler rod to pack refuse into top. Our patented smart technology will find a new, usable form for the matter you insert. 

Thank you for recycling!</pre>
{{< /rawhtml >}}

