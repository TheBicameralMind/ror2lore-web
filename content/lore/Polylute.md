---
title: Polylute
image: /img/Polylute.png
tags:
- Damage Items
---

{{< figure src="/img/Polylute.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Welcome to DataScraper (v3.1.53 – beta branch)
$ Scraping memory... done.
$ Resolving... done.
$ Combing for relevant data... done.
Complete!</span>

I've made a fas<span>---</span>ating discove-y! Deep in this strange <span>-----</span>, I found some carvings on the c<span>--</span>ffs<span>---</span>. It appears to be... music! There's n<span>----</span>ng else <span>--</span> could be! I'll att-mp- to t-ansc-be it, thou<span>--</span> the i-terf<span>--</span>nc- may <span>--</span>ddy <span>--</span> transmissions. Ahem...

d, d, d, a, G. A-e <span>---</span> g-t-ing this? Th<span>---</span>'s m<span>--</span>e.

<span>----</span> <span>---</span> - - <span>--</span> - <span>--------</span> - - - - - <span>-----</span> - - <span>--</span> - - - <span>--</span> - - -

How catchy!</pre>
{{< /rawhtml >}}

