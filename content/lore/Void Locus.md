---
title: Void Locus
image: /img/Void_Locus.png
tags:
- Environments
---

{{< figure src="/img/Void_Locus.png" width="50%" >}}

{{< rawhtml >}}
<pre class="lore">I just saw a portal and I just… decided to step through. Why? Because I’m a moron, that’s why.

The fog keeps it stable – the cells and the joints. The cradles. They like the fog. 

I’ve tried choking them – stop the fog- but they last a while. A lot longer than I’d survive in the fog. Couple hours, maybe.

The cradle goo is sustainable – I feed it, and in return it gives me food. Tastes like clam chowder, sorta.

I see things come and go, ferried by giant beasts. When they swim close, I can see inside. I’ve seen creatures and weapons and ships – ferried from the dark end to the light. Seen planets and moons, too. Not sure where they’re taking it all. Swear I saw a CHEF bot once.

Anyways, uh… where do I go from here?</pre>
{{< /rawhtml >}}

