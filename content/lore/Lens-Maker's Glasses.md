---
title: Lens-Maker's Glasses
image: /img/Lens-Maker's_Glasses.png
tags:
- Damage Items
---

{{< figure src="/img/Lens-Maker's_Glasses.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">…What I didn’t see was that the golem was aiming its laser at my back. I managed to barely dodge it - just by pure luck - but the blast knocked me to the ground. My glasses flew off my face and I was on my back, stunned, barely able to make sense of myself…

And then... well, I can’t really explain what happened next. Adrenaline, some kind of divine intervention, I dunno. But I reached out, and managed to grab my glasses, raise ‘em up to my eyes so I could see, and BAM! Fired the last of my bullets.

And, well... I guess I managed to hit something vital in its head. Exploded in red stone and glass. It stopped moving after that. 

If it wasn’t for that shot, I wouldn’t be here telling this story.”

-Signal echoes, UES Contact Light</pre>
{{< /rawhtml >}}

