---
title: Spare Drone Parts
image: /img/Spare_Drone_Parts.png
tags:
- Damage Items
---

{{< figure src="/img/Spare_Drone_Parts.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">//<span>--</span>AUTO-TRANSCRIPTION FROM UES [Redacted] <span>--</span>//</span>

"Y'see those new drone models they approved for us? Never thought I'd see 'em in person, if I'm being honest."

"Yeah, for sure. Only the big wigs had access to tech like that... Makes you wonder what we're up against. I mean, everyone's heard the stories, but..."

"Ah, I wouldn't worry. After all, y'know my buddy Jim?"

"From hardware?"

"Yeah, him. Dude's a savant, let me tell you. He got fired from every other branch he worked at for "unlawful modification of company property," and "weapons systems beyond what's legal for citizens to own"."

"Huh. Crazy."

"Mhm. I recommended him for this mission, since I figured we'd need everything we could get our hands on... Luckily, the Captain saw his potential. I just, uh..."

[Fire alarms begin to sound as a fire breaks out in Workshop 23-B. Drones, equipped with buzzsaws, chase technicians down the hall]

"...Hope he doesn't mind the noise."</pre>
{{< /rawhtml >}}

