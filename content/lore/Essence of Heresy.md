---
title: Essence of Heresy
image: /img/Essence_of_Heresy.png
tags:
- Damage Items
---

{{< figure src="/img/Essence_of_Heresy.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“…and her heart, too wicked and full of hate, was left where she once stood – at the site of her betrayal.”

-The Evisceration of Kur-skan the Heretic, VI</pre>
{{< /rawhtml >}}

