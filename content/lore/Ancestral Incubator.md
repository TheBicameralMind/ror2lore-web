---
title: Ancestral Incubator
image: /img/Ancestral_Incubator.png
tags:
- Unreleased
---

{{< figure src="/img/Ancestral_Incubator.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"How do they know who their leader is? Is it instinct? Or is there something about me?"

"I can't believe you're having me cover for you, Ashley. You know the rules. The last time someone brought something 'friendly' to the camp it did NOT end well. Rich cried for days."

"But if we figure out how to communicate we can use it to our benefit! Besides, the guy is pretty helpful around here. He can lift heavy boxes and reach high places."

"I think we should just put that thing back where you found it. Who knows if it will attract more?"

"This is for science...! And besides, maybe it can teach us something about this environment. He's a native here."

"Yeah, cool, native, okay, I'm out."</pre>
{{< /rawhtml >}}

