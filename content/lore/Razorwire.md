---
title: Razorwire
image: /img/Razorwire.png
tags:
- Damage Items
---

{{< figure src="/img/Razorwire.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Order: Custom-made Barbed Wire, 200m
Tracking Number: 05***********
Estimated Delivery: 02/01/2056
Shipping Method:  Priority
Shipping Address: PO Box 23-5B, Fort Blondershire, Colony of Man
Shipping Details: 

You were right to come to me for this PREMIUM barbed wire. The other retail models won’t get the job done like I do. Just cutting up invaders isn’t enough – no sir - I built in a secret defense mechanism!

By cramming as many razors, exacto knives, and any other blade I could find into this thing, this beauty is a powder keg just WAITING to burst! This is my greatest work yet. 

If I don’t get good reviews, expect to hear from my lawyers!</pre>
{{< /rawhtml >}}

