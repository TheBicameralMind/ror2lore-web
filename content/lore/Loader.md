---
title: Loader
image: /img/Loader.png
tags:
- Survivors
---

{{< figure src="/img/Loader.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">A lot of people would say that loading cargo is a dull job. The Loader could not disagree more.

Other jobs don’t supply their employees with a fully articulate titanium exoskeleton capable of lifting 250 tons.

Other jobs don’t allow their employees to meet the crews of countless starships from across the galaxy.

Other jobs don’t let you use your exoskeleton for your own entertainment, much less outfit it with custom-built grappling hooks powered by a winch capable of hauling up to 100 tons.

Other jobs don’t involve being approached by a legend among the UES work force, and asked to embark on a classified mission to the Uncharted Territories.

Other jobs don’t involve fighting exotic and strange alien creatures, and exploring mystical ruins on another planet.

A lot of people would say that loading cargo is a dull job. The Loader could not disagree more.</pre>
{{< /rawhtml >}}

