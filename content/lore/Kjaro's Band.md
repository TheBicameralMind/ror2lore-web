---
title: Kjaro's Band
image: /img/Kjaro's_Band.png
tags:
- Damage Items
---

{{< figure src="/img/Kjaro's_Band.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"Should passion die down,
Should light be extinguished,
Will you bring me patience?
Will you die with me?"

-The Syzygy of Io and Europa</pre>
{{< /rawhtml >}}

