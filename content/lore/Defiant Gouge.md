---
title: Defiant Gouge
image: /img/Defiant_Gouge.png
tags:
- Utility Items
---

{{< figure src="/img/Defiant_Gouge.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">Excerpt from the folk tale “Hymn of Aphocles”:

“...The statue had been carved to the artist’s creative vision. Beautiful and powerful, its mighty marble visage shone as radiantly as the moon. However, the common folk did not share the artist’s vision. Declaring the statue imperfect, a single laborer took up his gouge and defiled the work, shaping it to his own selfish ideas of beauty.

Admiring his own work, the laborer confidently claimed that he had increased the beauty of the art tenfold. However, the artist did not hear these words. The artist felt only rage, rage at the laborer who had defiled his masterpiece, and rage at the common folk who lauded the laborer as a hero.

And just as the laborer did not recognize the beauty of the untouched statue, so too did the laborer fail to recognize the shards of marble constricting around his leg.”</pre>
{{< /rawhtml >}}

