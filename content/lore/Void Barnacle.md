---
title: Void Barnacle
image: /img/Void_Barnacle_-_Logbook_Model.jpg
tags:
- Monsters
---

{{< figure src="/img/Void_Barnacle_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore">&gt; Automated report 2c150f1430ad6430161e4 is now available from site record 108gnasnoⒾ45.
&gt; Please refer to record 9494ba7sf77eef for additional details during your review.
&gt; Report Type: Machine-generated Transcription&gt; base Vernacular Profile |||||||||
&gt; - Source: 2c150f1430ad6430161e4 (Cosmic Noise, Recorded in Stratosphere of |||||||||||)
&gt; Priority: High
&gt; Report Content: 

<span>--</span> Beginning of Excerpt Flagged for Review –

...

Traveling through the [Void] is simple and safe with this easy process.

First things first, enable your [engine]. Wait approximately 34 cycles until it produces a [field]. This will allow you to cross the [planes].

When calibrating your engine, set your target to [NULL]. Do not worry about any turbulence you experience while crossing the [planes]. Do not worry about flashing lights. Do not worry about any creatures you may feel have boarded your vessel. All is safe and secure.

Once you access the [Void], please sit back and relax; the [Void] is a safe and secure environment. Agents will be deployed to escort you to your [cell]. Do not resist; our agents are here for your safety. 

Do not worry about any of our premier fauna you see in the distance.

Do not worry about the infinite sky, and do not worry about any panic attacks you have while attempting to percieve it.

Do not worry about the [barnacles] growing on the hull of your vessel.

Do not worry. We are here for you.

...

<span>--</span> End of Recording –

<span>--</span> End of Excerpt Flagged for Review –

&gt; TRANSLATION ERRORS: Too many to count

&gt; Please refer to report a13b2g8901cf2376102e for full audio excerpt.
<span>===================================================</span></pre>
{{< /rawhtml >}}

