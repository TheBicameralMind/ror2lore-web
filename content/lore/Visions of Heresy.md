---
title: Visions of Heresy
image: /img/Visions_of_Heresy.png
tags:
- Damage Items
---

{{< figure src="/img/Visions_of_Heresy.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"…and for her betrayal, and her lies, and her scheming ways, the Heretic was violently separated into four distinctive pieces, each to be scattered across the farthest depths of the Moon. 

First, her many eyes were plucked from her skull and sealed in boiling glass, forced to gaze upon her failure...”

-The Evisceration of Kur-skan the Heretic, III</pre>
{{< /rawhtml >}}

