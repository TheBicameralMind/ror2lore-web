---
title: Engineer
image: /img/Engineer.png
tags:
- Survivors
---

{{< figure src="/img/Engineer.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">“The future is now! Cybernetic enhancements – buy them cheap and become the person you’ve always dreamed of being!” 

Cybernetic enhancements were all the rage these days, especially since they were made available for public use. Initially, only military and commercial projects could license the procedures necessary to install cybernetic implants. But those days were long gone, and the concept of “the new human” was taking the galaxy by storm. 

Of course, with any movement, there was backlash.  What of the sanctity of the human body? What about taking pride in your biology? Such arguments were normally laughed off. “Get with the times,” people would say. But one man among many stood out, gaining public attention and starting a counter movement of anti-cybernetics.

The man postulated that mankind was walking on a tight rope – when do the cybernetics end, and where does the human begin? Will mankind end up as a machine species, or will we use our new tech to only better ourselves - without losing sight of what makes us human? The words of this man echoed across the galaxy, leaving a trail of revolution in its wake.</pre>
{{< /rawhtml >}}

