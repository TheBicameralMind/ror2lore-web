---
title: Scavenger
image: /img/Scavenger_-_Logbook_Model.jpg
tags:
- Bosses
---

{{< figure src="/img/Scavenger_-_Logbook_Model.jpg" width="40%" >}}

{{< rawhtml >}}
<pre class="lore"><span>========================================</span>
<span>====</span>   MyBabel Machine Translator   <span>====</span>
<span>====</span>     [Version 12.45.1.009 ]   <span>======</span>
<span>========================================</span>
Training… &lt;100000000 cycles&gt;
Training… &lt;100000000 cycles&gt;
Training... &lt;100000000 cycles&gt;
Training... &lt;102515 cycles&gt;
Complete!
Display result? Y/N
Y
<span>========================================</span>

I watch the strange creatures from a distance. 

What powerful things they use. I want them. But there’s too many of them. I’ve seen what they do to the others – they kill without mercy.

I’m a bit scared, but it’s okay. 

My things bring me comfort. 

My favorite thing is a little effigy, made of cloth and fluff. It’s a bit rough around the edges, but it’s okay. 

Holding it close brings me comfort when I’m scared. 

It helps me be brave, and sometimes I need to be brave to get the things I want.

The others say that these creatures came down from the sky, bringing so many things with them. They probably have so many things, back at their homes. I want those things. They don’t need all those things.

Now is the time to be brave.</pre>
{{< /rawhtml >}}

