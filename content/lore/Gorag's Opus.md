---
title: Gorag's Opus
image: /img/Gorag's_Opus.png
tags:
- Equipment
---

{{< figure src="/img/Gorag's_Opus.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore"><span class="mono">Audio transcription complete from "Carrion Crows Tour 2055: Special Edition"

Printing...</span>

"Halfway through the tour we were at this little shop down in Groveside and the guy at the desk is trying to sell us random junk."

"Yeah, he was totally out of the loop, had no idea who we were."

"Yep, anyways, as a joke, I’m thinking I’ll buy this ancient looking drum and use it on stage at the next show. Then we’d circle back and show this guy a video of his merch being used in the biggest performance on the planet."

"We tried to find him again, but the shop was all shut down. Couldn’t figure out what happened to him."

"Thing is, that drum drove our fans wild. It turned out to be our most successful tour ever."</pre>
{{< /rawhtml >}}

