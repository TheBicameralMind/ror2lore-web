---
title: Benthic Bloom
image: /img/Benthic_Bloom.png
tags:
- Utility Items
---

{{< figure src="/img/Benthic_Bloom.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"Luck? Who needs luck, in a place like this.

To survive here, you'd need more than just luck. You need intuition. Instinct. The right tools. Power.

I know you're scared. And I am too. But if there's anything this hell hole has taught me...

We make our own luck."

- Signal Echoes, UES Contact Light</pre>
{{< /rawhtml >}}

