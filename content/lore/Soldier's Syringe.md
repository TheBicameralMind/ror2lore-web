---
title: Soldier's Syringe
image: /img/Soldier's_Syringe.png
tags:
- Damage Items
---

{{< figure src="/img/Soldier's_Syringe.png" width="20%" >}}

{{< rawhtml >}}
<pre class="lore">"I ain't much for drugs... but hell, when fortune is knockin', ya gotta greet that door with a smile and a nod. Salud!"

-Signal echoes, UES Contact Light</pre>
{{< /rawhtml >}}

